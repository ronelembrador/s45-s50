import {Fragment} from 'react'
import {Container} from 'react-bootstrap'
import AppNavBar from './components/AppNavBar'
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import Courses from './pages/Courses'
import Home from './pages/Home'
import './App.css';

function App() {
  return (
  <Fragment>
    <AppNavBar />
    <Container>  
      {/*<Banner/>
      <Highlights/>*/}
      <Home/>
      <Courses/>
    </Container>
  </Fragment>
  );
}

export default App;
